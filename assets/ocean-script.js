//console.log('Ocean Script');
if($('.plastic-action').length) {
  const $popup = $('.plastic-action .video-popup');
  const $modal = $('.plastic-action #modal');
  const $closeIcon = $('.plastic-action .close');
  const $watchMoreLink = $('.plastic-action .video-play-link');

  $watchMoreLink.click(function(e) {
      $popup.fadeIn(200);
      $modal.fadeIn(200);
      e.preventDefault();
  });
  $closeIcon.click(function () {
      $popup.fadeOut(200);
      $modal.fadeOut(200);
  });
  // for escape key
  $(document).on('keyup',function(e) {
      if (e.key === "Escape") {
          $popup.fadeOut(200);
          $modal.fadeOut(200);
      }
  });

    // click outside of the popup, close it
    $modal.on('click', function(e){
        $popup.fadeOut(200);
        $modal.fadeOut(200);
    });
}


  $(document).on('submit','.search-form', function(e){
    console.log('submit');
    e.preventDefault();
    const query = $('.faq-search-input').val();
    if(query == '') {
      $('.accordion-item').show()
    } else {
      $('.accordion-item').hide();
      $(`[data-accordion-title*="${ query }"]`).show();
    }
  })

  $('.bg-image-slider').length && $('.bg-image-slider').slick({
    dots: false,
    arrows: false,
    slidesToShow: 1,
    autoplay: false,
    autoplaySpeed: 3000,
  })

  $('.partner-icons').slick({
    dots: false,
    arrows: false,
    slidesToShow: 4,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        }
      }
    ]
  })

  $('.how_it_works .bottom-icons').slick({
    dots: false,
    arrows: false,
    slidesToShow: 7,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 4,
        }
      }
    ]
  })

  $('[data-scroll-down]').on('click', function(){
    console.log($(this).closest('.shopify-section').height())
    $('html, body').animate({
      scrollTop: $(this).closest('.shopify-section').height()
    }, 200);
  })


  $(document).on('click', '[data-partner-icon]', function(e){
    const partnerName = $(this).data('partner-icon');
    $('[data-partner-details]').hide();
    $(`[data-partner-details="${ partnerName }"]`).show();

  })

  $(document).on('click', '[data-how-works-icon]', function(e){
    const partnerName = $(this).data('how-works-icon');
    $('[data-how-works-icon]').removeClass('active');
    $(this).addClass('active');
    $('[data-how-works-detail]').hide();
    $(`[data-how-works-detail="${ partnerName }"]`).show();
    $('[data-how-works-image]').hide();
    $(`[data-how-works-image="${ partnerName }"]`).show();
  })

  $('.partner-icons').on('afterChange', function(){
    $('.partner-icons .slick-current .partner-icon').trigger('click');
  })

  $('.accordion-title').on('click', function(){
    const currentItem = $(this).closest('.accordion-item');
    if(currentItem.hasClass('active')) {
      currentItem.removeClass('active')
    } else {
      $(this).closest('.accordion').find('.accordion-item').removeClass('active');
      currentItem.addClass('active');
    }
  })

  $('[data-partner-slide-prev]').on('click', function(){
    $('.partner-icons').slick('slickPrev')

  })

  $('[data-partner-slide-next]').on('click', function(){
    $('.partner-icons').slick('slickNext');
  })





window.onscroll = function() {myFunction()};
var header = document.getElementById("shopify-section-static-header");
// var sticky = header.offsetTop;
var sticky = 0;
if(document.querySelector('.pxs-announcement-bar')) {
  sticky = document.querySelector('.pxs-announcement-bar').clientHeight;
}

function myFunction() {
  console.log("sticky", sticky);
  if (window.pageYOffset > sticky) {
    header.classList.add("fixed");
  } else {
    header.classList.remove("fixed");
  }
}

function openTab(evt, tabId) {
  var i, tabcontent, tablinks;
  var tabPan = document.getElementById(tabId).closest('.tab-section');
  tabcontent = tabPan.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = tabPan.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    console.log(tablinks[i]);
    tablinks[i].className = tablinks[i].className.replace("active", "");
  }
  document.getElementById(tabId).style.display = "block";
  evt.currentTarget.className += " active";
}

var scrollpos = window.scrollY
var impact = document.querySelector(".impact-section")
var impact_height = impact.offsetTop - (window.outerHeight/2);
var circles = impact.getElementsByClassName('circle')
var add_class_on_scroll = () => {
  for (i = 0; i < circles.length; i++) {
    circles[i].classList.add("circle-animation");
  }
}
// var remove_class_on_scroll = () => {
//   for (i = 0; i < circles.length; i++) {
//     circles[i].classList.remove("circle-animation");
//   }
// }

window.addEventListener('scroll', function() {
  scrollpos = window.scrollY;

  if (scrollpos >= impact_height) { add_class_on_scroll() }
//   else { remove_class_on_scroll() }

//   console.log(scrollpos)
})